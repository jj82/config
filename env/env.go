package env

import (
	"os"
	"strings"

	proto "gitlab.bcowtech.de/bcow-go/structprototype"
)

const (
	TagName = "env"
)

func Process(prefix string, v interface{}) error {
	if len(prefix) > 0 {
		prefix += "_"
	}

	prototype, err := proto.Prototypify(v, &proto.PrototypifyConfig{
		TagName:              TagName,
		BuildValueBinderFunc: proto.BuildStringArgsBinder,
	})
	if err != nil {
		return err
	}

	var table proto.Values = make(proto.Values)
	for _, e := range os.Environ() {
		parts := strings.SplitN(e, "=", 2)
		name, value := parts[0], parts[1]
		if strings.HasPrefix(name, prefix) {
			table[name[len(prefix):]] = value
		}
	}
	err = prototype.BindValues(table)
	if err != nil {
		return err
	}
	return nil
}
