package config

import (
	"fmt"
	"io/ioutil"
	"os"

	"gitlab.bcowtech.de/bcow-go/config/env"
	"gitlab.bcowtech.de/bcow-go/config/flag"
	"gitlab.bcowtech.de/bcow-go/config/json"
	"gitlab.bcowtech.de/bcow-go/config/resource"
	"gitlab.bcowtech.de/bcow-go/config/yaml"
)

type UnmarshalFunc func(buffer []byte, v interface{}) error

type ConfigurationService struct {
	v interface{}
}

func NewConfigurationService(v interface{}) *ConfigurationService {
	service := ConfigurationService{
		v: v,
	}
	return &service
}

func (service *ConfigurationService) LoadEnvironmentVariables(prefix string) *ConfigurationService {
	err := env.Process(prefix, service.v)
	if err != nil {
		panic(fmt.Errorf("config: %v\n", err))
	}
	return service
}

func (service *ConfigurationService) LoadCommandArguments() *ConfigurationService {
	err := flag.Process(service.v)
	if err != nil {
		panic(fmt.Errorf("config: %v\n", err))
	}
	return service
}

func (service *ConfigurationService) LoadJsonFile(filepath string) *ConfigurationService {
	err := json.LoadFile(filepath, service.v)
	if err != nil && os.IsExist(err) {
		panic(fmt.Errorf("config: %v\n", err))
	}
	return service
}

func (service *ConfigurationService) LoadJsonBytes(buffer []byte) *ConfigurationService {
	err := json.LoadBytes(buffer, service.v)
	if err != nil {
		panic(fmt.Errorf("config: %v\n", err))
	}
	return service
}

func (service *ConfigurationService) LoadYamlFile(filepath string) *ConfigurationService {
	err := yaml.LoadFile(filepath, service.v)
	if err != nil && os.IsExist(err) {
		panic(fmt.Errorf("config: %#v\n", err))
	}
	return service
}

func (service *ConfigurationService) LoadYamlBytes(buffer []byte) *ConfigurationService {
	err := yaml.LoadBytes(buffer, service.v)
	if err != nil {
		panic(fmt.Errorf("config: %v\n", err))
	}
	return service
}

func (service *ConfigurationService) LoadResource(baseDir string) *ConfigurationService {
	err := resource.Process(baseDir, service.v)
	if err != nil {
		panic(fmt.Errorf("config: %v\n", err))
	}
	return service
}

func (service *ConfigurationService) LoadFile(fullpath string, unmarshal UnmarshalFunc) *ConfigurationService {
	path := os.ExpandEnv(fullpath)
	buffer, err := ioutil.ReadFile(path)
	if err != nil && os.IsExist(err) {
		panic(fmt.Errorf("config: %#v\n", err))
	}

	err = unmarshal(buffer, service.v)
	if err != nil {
		panic(fmt.Errorf("config: %#v\n", err))
	}
	return nil
}
