module gitlab.bcowtech.de/bcow-go/config

go 1.14

require (
	gitlab.bcowtech.de/bcow-go/structprototype v1.3.0
	gopkg.in/yaml.v2 v2.4.0
)
