package flag

import (
	"flag"

	proto "gitlab.bcowtech.de/bcow-go/structprototype"
)

const (
	TagName = "arg"
)

var (
	help = flag.Bool("help", false, "Show this help")

	provider = &flagBindingProvider{}
)

func Process(v interface{}) error {
	err := proto.Bind(v, &proto.PrototypifyConfig{
		TagName: TagName,
	}, provider)
	if err != nil {
		return err
	}

	return nil
}
