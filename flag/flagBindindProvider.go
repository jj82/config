package flag

import (
	"flag"
	"os"
	"reflect"

	proto "gitlab.bcowtech.de/bcow-go/structprototype"
)

var _ proto.BindingProvider = new(flagBindingProvider)

type flagBindingProvider struct{}

func (p *flagBindingProvider) BeforeBind(context *proto.PrototypeContext) error {
	return nil
}

func (p *flagBindingProvider) BindField(field proto.PrototypeField, rv reflect.Value) error {
	value := p.makeFlagValue(rv)
	flag.Var(value, field.Name(), field.Desc())
	return nil
}

func (p *flagBindingProvider) AfterBind(context *proto.PrototypeContext) error {
	// NOTE: ignore validate
	if !flag.Parsed() {
		flag.Parse()
	}

	if *help {
		flag.Usage()
		os.Exit(0)
	}
	return nil
}

func (p *flagBindingProvider) makeFlagValue(rv reflect.Value) flag.Value {
	if rv.CanInterface() {
		if rv.CanAddr() {
			rv = rv.Addr()
		}
		v := rv.Interface()
		switch v.(type) {
		case flag.Value:
			value, ok := v.(flag.Value)
			if ok {
				return value
			}
		}
	}
	return &flagValue{rv}
}
