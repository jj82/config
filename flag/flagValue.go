package flag

import (
	"flag"
	"reflect"

	proto "gitlab.bcowtech.de/bcow-go/structprototype"
)

var _ flag.Value = new(flagValue)

type flagValue struct {
	value reflect.Value
}

func (fv *flagValue) String() string {
	return fv.value.String()
}

func (fv *flagValue) Set(v string) error {
	return proto.StringArgsBinder(fv.value).Bind(v)
}
