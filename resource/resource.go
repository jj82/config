package resource

import (
	"os"

	proto "gitlab.bcowtech.de/bcow-go/structprototype"
)

const (
	TagName = "resource"
)

func Process(baseDir string, v interface{}) error {
	baseDir = os.ExpandEnv(baseDir)
	if len(baseDir) > 0 {
		// exist path
		if _, err := os.Stat(baseDir); os.IsNotExist(err) {
			return nil
		}
	}

	err := proto.Bind(v, &proto.PrototypifyConfig{
		TagName:           TagName,
		StructTagResolver: resourceTagResolver,
	}, &resourceBindingProvider{
		BaseDir: baseDir,
	})
	if err != nil {
		return err
	}

	return nil
}
