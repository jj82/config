package resource

import (
	"io/ioutil"
	"os"
	"path"
	"reflect"

	proto "gitlab.bcowtech.de/bcow-go/structprototype"
)

var (
	typeOfByteArray = reflect.TypeOf([]byte{})
)

type resourceBindingProvider struct {
	BaseDir string
}

func (p *resourceBindingProvider) BeforeBind(context *proto.PrototypeContext) error {
	return nil
}

func (p *resourceBindingProvider) BindField(field proto.PrototypeField, rv reflect.Value) error {
	filename := path.Join(p.BaseDir, field.Name())

	fileinfo, err := os.Stat(filename)
	if err != nil {
		if os.IsNotExist(err) {
			return nil
		}
		return err
	}
	if fileinfo.Mode().IsRegular() {
		buffer, err := ioutil.ReadFile(filename)
		if err != nil {
			return err
		}

		switch rv.Type() {
		case typeOfByteArray:
			rv.Set(reflect.ValueOf(buffer))
			return nil
		}
		return proto.BytesArgsBinder(rv).Bind(buffer)
	}
	return nil
}

func (p *resourceBindingProvider) AfterBind(context *proto.PrototypeContext) error {
	return nil
}
