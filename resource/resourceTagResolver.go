package resource

import (
	"strings"

	proto "gitlab.bcowtech.de/bcow-go/structprototype"
)

func resourceTagResolver(fieldname, token string) (*proto.StructTag, error) {
	if len(token) > 0 {
		parts := strings.SplitN(token, ";", 2)
		var desc string
		if len(parts) == 2 {
			parts, desc = strings.Split(parts[0], ","), parts[1]
		} else {
			parts = strings.Split(token, ",")
		}
		name, flags := parts[0], parts[1:]

		var tag *proto.StructTag
		if len(name) > 0 && name != "-" {
			tag = &proto.StructTag{
				Name:  name,
				Flags: flags,
				Desc:  desc,
			}
		}
		return tag, nil
	}
	return nil, nil
}
