package yaml

import (
	"io/ioutil"
	"os"

	"gopkg.in/yaml.v2"
)

func LoadFile(filepath string, v interface{}) error {
	path := os.ExpandEnv(filepath)
	buffer, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}

	return LoadBytes(buffer, v)
}

func LoadBytes(buffer []byte, v interface{}) error {
	err := yaml.Unmarshal(buffer, v)
	if err != nil {
		return err
	}
	return nil
}
